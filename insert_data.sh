#! /bin/bash

if [[ $1 == "test" ]]
then
  PSQL="psql --username=postgres --dbname=worldcuptest -t --no-align -c"

else
  PSQL="psql --username=freecodecamp --dbname=worldcup -t --no-align -c"
fi

# Do not change code above this line. Use the PSQL variable above to query your database.
  #parsing and insert
echo $($PSQL "\i create.sql")

cat games.csv | while IFS=',' read year round winner opponent winner_goals opponent_goals
do
  echo $year, $round, $winner, $opponent, $winner_goals, $opponent_goals
  if [[ $winner != "winner" ]]
  then
  # get check if winner already in
    wID=$($PSQL "SELECT team_id FROM teams WHERE name='$winner'")
  #if doesn't exist
    if [[ -z $wID ]]
    then
      winner_add_result=$($PSQL "INSERT INTO teams(name) VALUES('$winner')")
      if [[ $winner_add_result == "INSERT 0 1" ]]
      then
        echo Inserted into teams, $winner
        wID=$($PSQL "SELECT team_id FROM teams WHERE name='$winner'")
      fi
    fi
  opponent_id=$($PSQL "SELECT team_id FROM teams WHERE name='$opponent'")
  #if doesn't exist
    if [[ -z $opponent_id ]]
    then
      opponent_add_result=$($PSQL "INSERT INTO teams(name) VALUES('$opponent')")
      if [[ $opponent_add_result == "INSERT 0 1" ]]
      then
        echo Inserted into teams, $opponent
          opponent_id=$($PSQL "SELECT team_id FROM teams WHERE name='$opponent'")
      fi
    fi

    game_id=$($PSQL "SELECT game_id FROM games WHERE year = $year AND round = '$round' AND winner_id = $wID AND opponent_id = $opponent_id AND winner_goals = $winner_goals AND opponent_goals = $opponent_goals")
    if [[ -z $game_id ]]
    then
      game_add_result=$($PSQL "INSERT INTO games(year, round, winner_id, opponent_id, winner_goals, opponent_goals) VALUES($year, '$round' , $wID, $opponent_id, $winner_goals, $opponent_goals)")
      if [[ $winner_add_result == "INSERT 0 1" ]]
      then
        echo Inserted into games, year: $year, round: $round, winner: $wID, opp: $opponent_id, w goals: $winner_goals, opp goals: $opponent_goals
        #game_id=$($PSQL "SELECT game_id FROM games WHERE year = $year AND round = '$round' AND winner_id = $wID AND opponent_id = $opponent_id AND winner_goals = $winner_goals AND opponent_goals = $opponent_goals")
      fi
    fi
  fi
done
